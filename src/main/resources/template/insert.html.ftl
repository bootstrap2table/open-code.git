<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>添加${classname}</title>
    <meta name="description" content="" />
    <meta name="author" content="pc" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale=1, minimum-scale=1, user-scalable=no" />
    <meta name="screen-orientation"content="portrait">
    <meta name="format-detection" content="telephone=no" />
    <#noparse><#include "/inc/common.html"></#noparse>
</head>
<body>
<div class="container">
    <div style="margin-bottom: 10px">
        <input type="button" value="返回" style="width: 80px; height: 30px; vertical-align: bottom; line-height: 10px;" class="btn-primary" onclick="${classname}Back()" />
        <input type="button" value="保存" style="width: 80px; height: 30px; vertical-align: bottom; line-height: 10px;" class="btn-primary" onclick="${classname}Save()" />
        <input type="button" value="取消" style="width: 80px; height: 30px; vertical-align: bottom; line-height: 10px;" class="btn-primary" onclick="${classname}Cancle()" />
    </div>
    <div>
        <form id="${classname}InsertFm" name="${classname}InsertFm"  method="post" action="">
            <table border="0" cellpadding="0" id="${classname}InsertTable" cellspacing="0"
                   class="formlist" style="width: 100%; table-layout: fixed;">
${insertFormParam}
            </table>
        </form>
    </div>
</div>
<script type="text/javascript">
    function ${classname}Save(){//保存
        if($('#${classname}InsertFm').valid()){//验证通过
            $.ajax({
                url: '<#noparse>${request.contextPath}</#noparse>/${classname}/insert',
                contentType:"application/json",
                dataType : "json",
                type : "put",
                data : JSON.stringify($('#${classname}InsertFm').serializeObject()),
                success: function(json, status) {
                    if(json==true){
                        layer.msg('添加成功', {
                            icon:1,
                            time: 500,
                            end: function(index, layero){
                                parent.layer.close(parent.layer.getFrameIndex(window.name));  // 关闭layer
                                window.parent.location.reload(); //刷新父页面
                            }
                        });
                    }else{
                        layer.msg('添加失败', {
                            icon:5,
                            time: 500,
                            end: function(index, layero){
                                parent.layer.close(parent.layer.getFrameIndex(window.name));  // 关闭layer
                                window.parent.location.reload(); //刷新父页面
                            }
                        });
                    }
                },
                error: function(json, status) {
                    layer.msg('系统异常,请稍后重试或联系技术人员', {
                        icon:5,
                        time: 1500,
                        end: function(index, layero){
                            parent.layer.close(parent.layer.getFrameIndex(window.name));  // 关闭layer
                            window.parent.location.reload(); //刷新父页面
                        }
                    });
                }
            });
        }
    }
    function ${classname}Cancle(){//取消
        $(':input','#${classname}InsertFm')
            .not(':button, :submit, :reset, :hidden')
            .val('')
            .removeAttr('checked')
            .removeAttr('selected');
    }
    function ${classname}Back(){//返回
        parent.layer.close(parent.layer.getFrameIndex(window.name));  // 关闭layer
        window.parent.location.reload(); //刷新父页面
    }
</script>
</body>
</html>
