package ${package}.${moduleName}.service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.qdone.framework.core.page.CoreUtil;
import com.qdone.framework.core.page.PageInfo;
import com.qdone.framework.core.page.PageList;
import ${package}.${moduleName}.dao.${className}Dao;
import ${package}.${moduleName}.model.${className};
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
* ${classname}服务接口
* ${comments}
* @author ${author}
* @email ${email}
* @date ${datetime}
*/
@Service("${classname}Service")
@Transactional(propagation = Propagation.REQUIRED,isolation = Isolation.DEFAULT)
public class ${className}Service{

@Autowired
private ${className}Dao ${classname}Dao;

/**
* 分页查询
*/
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public PageList<${className}> selectPage(${className} entity) {
    PageList<${className}> pageList = new PageList<${className}>();
    PageInfo pageInfo = CoreUtil.createBootStrapPage(entity, true);
    String orders = CoreUtil.createSort(pageInfo, true);
    PageHelper.startPage(pageInfo.getCurrentNumber(), pageInfo.getPageSize(),orders);
    List<${className}> list = ${classname}Dao.select(entity);
    // 设置总记录数
    pageList.setList(list);
    Page<${className}> pg = (Page<${className}>) list;
    pageInfo.setRecordCount(pg.getTotal());
    pageList.setPageInfo(pageInfo);
    return pageList;
}


/**
* 查询列表
* @param entity 查询参数
* @return 查询列表
*/
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public List<${className}> selectList(${className} entity) {
    return ${classname}Dao.select(entity);
}


/**
* 查询单项
* @param entity 查询参数
* @return 查询单项
*/
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public ${className} selectOne(${className} entity) {
    return ${classname}Dao.select(entity);
}

/**
* 查看
* @param pk 主键
* @return 根据主键查询结果
*/
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public ${className} view(${pk.attrType} pk) {
    return ${classname}Dao.view(pk);
}


/**
* 新增(主键可以使用数据库自增，也可以使用mybatis生成)
* @param entity 新增参数
* @return 新增结果,结果中含有主键，操作结果
*/
@Transactional(readOnly = false,rollbackFor = Exception.class)
public ${className} insert(${className} entity) {
    entity.setOperateResult(${classname}Dao.insert(entity));
    return entity;
}

/**
* 更新
* @param entity 更新参数
* @return 更新结果,结果中含有操作结果
*/
@Transactional(readOnly = false,rollbackFor = Exception.class)
public ${className} update(${className} entity){
    entity.setOperateResult(${classname}Dao.update(entity));
    return entity;
}

/**
* 添加或修改
* @param entity 数据参数
* @return 添加或修改
*/
@Transactional(readOnly = false,rollbackFor = Exception.class)
public int merge(${className} entity) {
    entity.setOperateResult(${classname}Dao.merge(entity));
    return entity;
}

/**
* 批量添加或修改
* @param arr 对象集合
* @return 批量添加或修改
*/
@Transactional(readOnly = false,rollbackFor = Exception.class)
public int batchMerge(List<${className}> arr) {
    return ${classname}Dao.batchMerge(arr);
}

/**
* 批量删除
* @param pkList 对象集合
* @return 批量删除集合参数
*/
@Transactional(readOnly = false,rollbackFor = Exception.class)
public int batchDelete(List<${className}> pkList) {
    return ${classname}Dao.batchDelete(pkList);
}


}
